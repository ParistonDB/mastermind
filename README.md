Projekt "Mastermind" pełni tutaj rolę podstrony, do której wymagane jest zalogowanie się. Można skorzystać z konta testowego login: **user**, hasło: **user**. Regulamin gry został przedstawiony na poniższych zdjęciach.

![mastermin1.png](https://bitbucket.org/repo/469Xeb/images/2757518859-mastermin1.png)

![mastermin2.png](https://bitbucket.org/repo/469Xeb/images/3279289557-mastermin2.png)

![mastermind3.png](https://bitbucket.org/repo/469Xeb/images/1099488261-mastermind3.png)