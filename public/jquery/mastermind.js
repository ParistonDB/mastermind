$().ready(function() {
  var sizeCount;
  var otrzymanyJSON;

  $("#play").click(function() {
    var play_values = {
      codeSize: $("#codeSize").val(),
      maxColors: $("#maxColors").val(),
      maxMoves: $("#maxMoves").val()
    };

    var reg = new RegExp('^[0-9]+$');


    if(!$("#codeSize").val().match(reg) && !$("maxColors").val().match(reg) && !$("maxMoves").val().match(reg)) {
      alert("1. Wszystkie pola wymagane.\n"+
            "2. Dozwolone tylko cyfry");
      return 0;
    }

    $.ajax({
      url: '/newgame',
      type: 'POST',
      data: JSON.stringify(play_values),
      contentType: 'application/json; charset=UTF-8',
      dataType: 'json',
      async: false,
      success: function() {}
    });

    $("#codeSize").prop('disabled', true);
    $("#maxColors").prop('disabled', true);
    $("#maxMoves").prop('disabled', true);
    $("#play").prop('disabled', true);

    sizeCount = parseInt(play_values.codeSize, 10);
    for(var i = 0; i < sizeCount; i++) {
      $("<input type='text' class='guess_input' id='game' />").insertAfter("#zgadula");
    }

    $("<div id='regulamin'><ul>"+
    "<li>Dozwolony zakres wpisywanych liczb: 0-"+$("#maxColors").val()+
    "</li><li>Dozwolone tylko cyfry</li></ul>"+
    "</div>").insertBefore("#wholeArea");

    $(".komunikat").hide();
    $("#guess").show();
    $("#replay").show();
  });

  $("#guess").click(function() {
    //czyszczenie
    $("body h3").each(function() {
      $(this).remove();
    });

    var inserted_values = {
      inserted: []
    };

    var range = $("#maxColors").val();

    var reg = new RegExp('^[0-'+range+']+$');

    $("#wholeArea #game").each(function() {
      if(!$(this).val().match(reg)) {
        $(this).addClass('uwaga');
      } else {
        $(this).removeClass('uwaga');
      }
      inserted_values.inserted.push($(this).val());
    });

    $.ajax({
      url: '/move',
      type: 'POST',
      data: JSON.stringify(inserted_values),
      contentType: 'application/json; charset=UTF-8',
      dataType: 'json',
      async: false,
      success: function(json) {
        otrzymanyJSON = json;
      }
    });

    var przegrana = false;
    if(otrzymanyJSON.fails === otrzymanyJSON.maxMoves) { przegrana = true; } else { przegrana = false; }
    var pomylka = false;
    if(otrzymanyJSON.points !== otrzymanyJSON.codeSize && otrzymanyJSON.fails < otrzymanyJSON.maxMoves) { pomylka = true; }
    else { pomylka = false; }
    var wygrana = false;
    if(otrzymanyJSON.points === otrzymanyJSON.codeSize) { wygrana = true; } else { wygrana = false; }
    if(przegrana) {
      $(this).prop('disabled', true);
    }

    if(pomylka) {
      $("<h3>Pomylka! Trafione: "+otrzymanyJSON.points+" Pozostalo prob: "+(otrzymanyJSON.maxMoves-otrzymanyJSON.fails)+"</h3>")
      .insertBefore("#wholeArea");
    } else if(przegrana) {
      $("<h3>Przegrales!</h3>").insertBefore("#wholeArea");
      $("#wholeArea input[type='text']").each(function() {
        $(this).prop('disabled', true);
      });
    } else if(wygrana) {
      $("<h3>Wygrana!!!</h3>").insertBefore("#wholeArea");
      $("#wholeArea input[type='text'], input[id='guess']").each(function() {
        $(this).prop('disabled', true);
      });
    }

    //blokowanie obecnie wykorzystanych inputow
    $("#wholeArea input[type='text']").each(function() {
      $(this).prop('disabled', true);
      $(this).addClass('blokada');
      $(this).attr('id', 'forget');
    });

    if(!wygrana && !przegrana) {
      $("<br />").insertAfter("#zgadula");
      for(var i = 0; i < sizeCount; i++) {
        $("<input type='text' class='guess_input' id='game' />").insertAfter("#zgadula");
      }
    }
  });

  $("#replay").click(function() {
    $("#codeSize").prop('disabled', false);   $("#codeSize").val("");
    $("#maxColors").prop('disabled', false);  $("#maxColors").val("");
    $("#maxMoves").prop('disabled', false);   $("#maxMoves").val("");
    $("#play").prop('disabled', false);
    $("#guess").prop('disabled', false);

    $("#guess").hide();
    $("#replay").hide();

    //czyszczenie
    $("body h3, input[class='guess_input'], input[class='guess_input uwaga'], input[class='guess input blokada uwaga'], input[class='guess_input blokada'], #regulamin").each(function() {
      $(this).remove();
    });
  });
});
