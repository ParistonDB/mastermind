$().ready(function() {
  $("#register").click(function() {

    var registration = {
      register_login: $("#register_login").val(),
      register_password: $("#register_password").val()
    };

    $.ajax({
      url: '/register',
      type: 'POST',
      data: JSON.stringify(registration),
      contentType: 'application/json; charset=UTF-8',
      dataType: 'json',
      async: false,
      success: function() {}
    });
  });
});
