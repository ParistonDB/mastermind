$().ready(function() {
  $("#rejestracja, #komunikat_rejestracja").click(function() {
    $(".magic").addClass("beksa");
    $(".register_form").addClass("beksa");
  });

  $("#logowanie, #komunikat_logowanie").click(function() {
    $(".magic").addClass("beksa");
    $(".login_form").addClass("beksa");
  });

  $(".magic").click(function() {
    $(".magic").removeClass("beksa");
    $(".register_form").removeClass("beksa");
    $(".login_form").removeClass("beksa");
  });

  $('.register_form').click(function(event){
    event.stopPropagation();
  });

  $('.login_form').click(function(event){
    event.stopPropagation();
  });
});
