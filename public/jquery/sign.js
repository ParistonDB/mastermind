$().ready(function() {
  $("#sign").click(function() {

    var signing = {
      login: $("#login").val(),
      password: $("#password").val()
    };

    $.ajax({
      url: '/sign',
      type: 'POST',
      data: JSON.stringify(signing),
      contentType: 'application/json; charset=UTF-8',
      dataType: 'json',
      async: false,
      success: function() {}
    });
  });
});
