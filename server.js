
/*******************************************/
/**/  var express         = require('express'),
/**/      cookieParser    = require('cookie-parser'),
/**/      session         = require('express-session'),
/**/      bodyParser      = require('body-parser'),
/**/      ejs             = require('ejs'),
/**/      mongoose        = require('mongoose'),
/**/      _               = require('underscore');
/*******************************************/

var game = {
  codeSize: 0,
  maxColors: 0,
  maxMoves: 0,
  secret: [],
  inserted: [],
  fails: 0,
  points: 0
};

var app = express();

mongoose.connect('mongodb://localhost/samurai');

var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function (callback) {
  console.log("Connected with database.");
});

var user = mongoose.model('users', new mongoose.Schema({
    _id:          String,
    name:         String,
    password:     String,
    age:          Number
}));

app.use(cookieParser());

app.use(session({
    store: require ('mongoose-session')(mongoose),
    secret: 'test session',
    resave: false,
    saveUninitialized: true
}));

app.use(express.static('public'));
app.use('/js', express.static('bower_components/jquery/dist'));
app.use(bodyParser.json());
app.set('view engine', 'ejs');
app.set('views', 'public/views');

app.use(function(req, res, next) {
  user.findOne({ name: req.body.login, password: req.body.password },
    function(err, user) {
      if(!user) {
        next();
      }
      else {
        console.log("User has been logged in.");
        req.session.user = req.body;
        next();
      }
  });
  console.log(req.session.user);
});

function checkAuth(req, res, next) {
  if(!req.session.user) {
    res.render('index');
  } else {
    next();
  }
}


app.get('/', checkAuth, function(req, res) {
    res.render('index', {user: req.session.user});
});

app.post('/register', function(req, res) {
    new user({
      _id:      mongoose.Types.ObjectId(),
      name:     req.body.register_login,
      password: req.body.register_password,
      age:      21
    }).save(function(err) {
      if(err) console.log(err);
      else console.log("A user has been added.");
    });
    res.end();
});

app.get('/register', checkAuth, function(req, res) {
    res.render('register');
});

app.post('/sign', function(req, res) {
    if(!req.session.user) {
      res.render('sign');
    }
    res.end();
});

app.get('/sign', function(req, res) {
  res.render('sign');
  //res.redirect('/');
  res.end();
});

app.get('/logout', function(req, res) {
  req.session.destroy();
  console.log("Usunieto sesje");
  res.redirect('/');
});

app.get('/users', function(req, res) {
    res.render('users');
});

app.get('/mastermind', checkAuth, function(req, res) {
    res.render('mastermind', {user: req.session.user});
});

app.post('/newgame', function(req, res, next) {
  game.secret = [];
  game.fails = 0;
  game.points = 0;
  game.inserted = [];

  if(game.hasOwnProperty("codeSize")) {
    game.codeSize = parseInt(req.body.codeSize, 10);
  }

  if(game.hasOwnProperty("maxColors")) {
    game.maxColors = parseInt(req.body.maxColors, 10);
  }

  if(game.hasOwnProperty("maxMoves")) {
    game.maxMoves = parseInt(req.body.maxMoves, 10);
  }

  for(var i = 0; i < game.codeSize; i++) {
    game.secret.push(Math.floor((Math.random() * game.maxColors) + 0));
  }
  req.session.game = game;

  console.log(game);
  next();
});

app.post('/move', function(req, res) {
    if(game.hasOwnProperty("inserted")) {

      for(var i = 0; i < req.body.inserted.length; i++) {
        req.session.game.inserted[i] = parseInt(req.body.inserted[i], 10);
      }
      //req.session.game.inserted = req.body.inserted;
    }

    var mark = function (code, move) {

      var calcBlack = function() {
        return _.size(_.filter(code, function(value, index) {
          return value === move[index];
        }));
      };

      var calculateWhites = function () {
        return _.countBy(code, function(num) {
          return num;
        });
      };

      var whiteFinder = function(code, move) {
        var appearancesMap = calculateWhites(code);
        return function(element) {
          if(appearancesMap[element] > 0){
            appearancesMap[element]--;
            return true;
          }
          return false;
        };
      };
      var calcWhite = function() {
        return _.filter(move, whiteFinder(code, move));
      };

      return {
        black: calcBlack(),
        white: calcWhite(code, move).length - calcBlack(code, move)
      };
    };

    req.session.game.points = mark(req.session.game.secret, req.session.game.inserted).black;

    if(req.session.game.points === req.session.game.codeSize) {
      console.log("Wygrales!");
    } else {
      req.session.game.fails++;
      console.log("Pomylka! Pozostale proby: "+(req.session.game.maxMoves-req.session.game.fails));
    }

    if(req.session.game.fails >= req.session.game.maxMoves) {
      console.log("Przegrales!");
    }
    console.log(req.session.game.inserted);
    res.json(req.session.game);
});

app.listen(8080, function() {
  //var port = arguments[0];
  console.log('http://localhost/8080');
});
